function Header() {
  return (
    <nav className='nav-wrapper'>
      <a href='#' className='brand-logo'>
        React Shop
      </a>
      <ul id='nav-mobile' className='right hide-on-med-and-down'>
        <li>
          <a href='#' className='Repo'></a>
        </li>
      </ul>
    </nav>
  );
}

export { Header };
