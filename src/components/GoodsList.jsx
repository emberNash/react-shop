import { GoodsItem } from './GoodsItem';
const GoodsList = (props) => {
  const { goods = [], addToBasket } = props;
  if (!goods.length) {
    return <h3>Nothing here</h3>;
  }

  return (
    <div className='goods'>
      {goods.slice(0, 10).map((item) => {
        return (
          <GoodsItem
            key={item.mainId}
            {...item}
            addToBasket={addToBasket}
          ></GoodsItem>
        );
      })}
    </div>
  );
};

export { GoodsList };
