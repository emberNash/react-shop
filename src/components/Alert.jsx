import { useState, useEffect } from 'react';

const Alert = (props) => {
  const { displayName, closeAlert } = props;

  useEffect(() => {
    const timerId = setTimeout(closeAlert, 1000);
    return () => {
      clearTimeout(timerId);
    };
  }, [displayName]);
  return (
    <div id='toast-container'>
      <div className='toast'>{displayName} add to cart</div>
    </div>
  );
};
export { Alert };
