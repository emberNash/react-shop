import './App.css';
import { Footer } from "./components/Footer";
import { Header } from "./components/Header";
import { Shop } from "./components/Shop";


function App() {
  return (
    <div className="App">
			<Header></Header>
			<Shop></Shop>
			<Footer></Footer>
    </div>
  );
}

export default App;
